
#include <iostream>
using namespace std;

void MERGE(int A[],int p ,int q ,int r)
{
    int i,j,k;
    int n1=q-p+1;
    int n2=r-q;
    int left[n1];
    int right[n2];

    for( i=0;i<n1;i++)
    {

        left[i]=A[p+i];

    }
    for( j=0;j<n2;j++)
    {
        right[j]=A[q+j+1 ];

    }
    i=0;
    j=0;

    for( k=p;i<n1 && j<n2;k++)
    {
      if(left[i]<right[j])
      {
          A[k]=left[i++];
      }
        else
      {
          A[k]=right[j++ ];
      }
     }

    while(i<n1)
    {
     A[k++]=left[i++];



    }
    while(j<n2)
    {

        A[k++]=right[j++];



    }
}

void MERGESORT(int A[],int p,int r)
{
 int q;
 if(p<r)
 {
     q=(p+r)/2;
     MERGESORT(A,p,q);
     MERGESORT(A,q+1,r);
     MERGE(A,p,q,r);

 }
}

int main()
{



    int Size;


     int i;
    cout<<"Enter number of elements :";

    cin>>Size;

    int A[Size];
    cout<<"Enter the array elements :";

    for( i=0;i<Size;i++)
    {
        cin>>A[i];
    }

    MERGESORT(A,0,Size-1);
    for( i=0;i<Size;i++)
    {
        cout<<A[i]<<" ";
    }

    cout<<endl;




}
